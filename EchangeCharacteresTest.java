package echangeCharacteres;

import static echangeCharacteres.EchangeCharacteres.removeAChar;

import static echangeCharacteres.EchangeCharacteres.swapChars;
import static org.junit.Assert.assertEquals;

public class EchangeCharacteresTest {
    public static void main(String[] args){
        assertEquals(removeAChar("ABCD"),"BCD");
        assertEquals(removeAChar("BBAA"),"BBAA");
        assertEquals(removeAChar("AACD"),"CD");
        assertEquals(removeAChar("BACD"),"BCD");
        assertEquals(removeAChar("AABAA"),"BAA");
        assertEquals(removeAChar("AB"),"B");
        assertEquals(removeAChar("B"),"B");
        assertEquals(removeAChar("A"),"");
        assertEquals(removeAChar(""),"");

        assertEquals(swapChars("AB"),"BA");
        assertEquals(swapChars("RAIN"),"RANI");
        assertEquals(swapChars("TENRAIN"),"TENRANI");
        assertEquals(swapChars("A"),"A");
        assertEquals(swapChars(""),"");
    }


}
