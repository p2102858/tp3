package echangeCharacteres;

public class EchangeCharacteres {

    public static String removeAChar(String str){
        if(str.length() > 1) return str.substring(0, 2).replaceAll("A","") +  str.substring(2);
        else return str.replaceAll("A","");
    }

    public static String swapChars(String str){
        if(str.length() > 1)return str.substring(0,str.length()-2)+str.charAt(str.length()-1)+str.charAt(str.length()-2);
        else return str;
    }
}
